import type { XYZ, Stage, Target, TypedArray } from '../lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, lerp, len2, iir, fmt } from '../lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from '../lib/log';
import {
  make2DTarget, make2DTTarget, getNorm,
  getMean, getScore, swapValues, getStdDev,
} from '../lib/target';
import {
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON, saveState,
} from '../lib/save';
import {
  allocStage,
  initEqual, checkHistogram, randomPoint,
  iterateIJK, iterateXYZ, iterateFXYZ, sampleXYZ,
  randomIndex, toIndex, fromIndex, wrapIndex,
  fft3d,
} from '../lib/stage';
import { parseNumber, parsePoint, parseSize } from '../lib/parse';
import { genRandom } from '../lib/gen';

////////////////////////////////////////////////////////////////////////////////

const lerpTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l ? lerp(l, f * mean, k) / l : 0;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const diffTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = (l - f * mean) / l;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const makeSwapper = (
  target: Target,
  norm: number,
) => {

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    const q = getScore(fs, size, mean, target, norm);
    return q;
  };

  const swap = (
    stage: Stage,
    k1: number,
    k2: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);
  }

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);

    const q = evaluateScore(stage);

    if (q < best) {
      return q;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swap, swapMaybe, evaluateScore};
}

const run = (
  stage: Stage,
  samples: number,
  retry: number,
  focus: number,

  steps: number,
  checkpoint: number,
  limit: number,
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swap, evaluateScore} = makeSwapper(target, norm);
  
  const start = +new Date();

  const logStep = makeLogStep();
  const logRate = makeLogRate();
  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  let best = initial;

  const order = seq(length);
    
  const eps = 0.00001;

  let success = 0;
  let failed = 0;
  
  let duds = new Set<number>();
  let peaks: number[] = [];
  
  let points: number[] = [];

  const getBiasedPoint = (flip: boolean = false) => () => {
    const i = Math.floor(Math.pow(Math.random(), 4) * order.length);
    return order[flip ? order.length - 1 - i : i];
  };
  
  const getRandomPoint = () => () => randomIndex(size);
  const getPeak = () => (i: number) => order[i];
  const getNoPeak = () => (i: number) => order[order.length - 1 - i];

  const ds2 = new Float32Array(ds.length);

  const tsv: any[] = ['x','y','z','min','max','avg','q','d(c)/d(n)','ifft(d(c)/d(s))', 'v1', 'v2'];
  console.log(tsv.join('\t'));

  fft3d(vs, fs, size);
  const mean = getMean(fs, size);

  lerpTarget(fs, size, mean, eps, target, norm);
  fft3d(fs, ds, size, { inverse: true });

  fft3d(vs, fs, size);
  diffTarget(fs, size, mean, eps, target, norm);
  fft3d(fs, ds2, size, { inverse: true });
  for (let i = 0; i < length; ++i) {
    const i2 = i * 2;
    ds2[i2] = ds2[i2];
    ds[i2] = ((ds[i2] - vs[i2]) / eps);
  }
  
  const seen = new Set();
  
  let accum = 0;
  
  //genRandom(stage, 100, 100);

  for (let i = 0; i < samples; ++i) {
    //let k1 = i;
    let k1 = randomIndex(size);
    let [x, y, z] = fromIndex(k1, size);
    
    if (seen.has(k1)) {
      --i;
      continue;
    }
    seen.add(k1);
    
    let better = 0;
    let worse = 0;
    let bestK = 0;
    let bestQ = 0;

    let min = 0;
    let avg = 0;
    let max = 0;
    let count = 0;
    
    iterateIJK(size, (x, y, z, k2) => {
      
      swap(stage, k1, k2);
      const q = evaluateScore(stage);
      swap(stage, k1, k2);
      
      if (q < best) {
        better++;
        if (q < bestQ) {
          bestK = k2;
          bestQ = q;
        }
      }
      else worse++;
    
      let d = best - q;
      min = Math.min(d, min);
      max = Math.max(d, max);
      avg += d;
      count++;
    });

    avg /= count;
    
    const q = better / (better + worse);
    const dcdn = ds[k1*2];
    const dcds = ds2[k1*2];

    const v1 = vs[k1*2];
    const v2 = vs[bestK*2];
    
    accum += max;
    console.log([x, y, z, min, max, avg, q, dcdn, dcds, v1, v2].join('\t'));
  }
  
  console.log('avg max =\t', accum / samples);
  
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('Start-up');

const defaults = {
  size: [64, 64, 16],
  steps: 10,
  checkpoint: 3,
  limit: 1000,

  samples: 128,
  retry: 4,
  focus: 1,
  
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) :resolved.size;
const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;

const samples = parseNumber(args.samples) || resolved.samples;
const retry = parseNumber(args.retry) || resolved.retry;
const focus = parseNumber(args.focus) || resolved.focus;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, steps, checkpoint, limit, samples, retry, focus, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, samples, retry, focus, steps, checkpoint, limit);

logTime('Save');

saveAll(stage, true);

logTime('Done');
