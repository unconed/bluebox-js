import type { XYZ, Stage, Target, TypedArray } from '../lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, lerp, len2, iir, fmt } from '../lib/math';
import { logTime, logScore } from '../lib/log';
import {
  make2DTarget, make2DTTarget, getNorm,
  getMean, getScore, swapValues, getStdDev,
} from '../lib/target';
import {
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON, saveState,
} from '../lib/save';
import {
  allocStage,
  initEqual, checkHistogram, randomPoint,
  iterateIJK, iterateXYZ, iterateFXYZ, sampleXYZ,
  randomIndex, toIndex, fromIndex, wrapIndex,
  fft3d,
} from '../lib/stage';
import { parseNumber, parsePoint, parseSize } from '../lib/parse';
import { genRandom } from '../lib/gen';

////////////////////////////////////////////////////////////////////////////////

const lerpTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l ? lerp(l, f * mean, k) / l : 0;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const diffTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l / mean - f;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const makeSwapper = (
  target: Target,
  norm: number,
) => {

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    const q = getScore(fs, size, mean, target, norm);
    return q;
  };

  const swap = (
    stage: Stage,
    k1: number,
    k2: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);
  }

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);

    const q = evaluateScore(stage);

    if (q < best) {
      return q;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swap, swapMaybe, evaluateScore};
}

const run = (
  stage: Stage,
  point: number[],
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swap, evaluateScore} = makeSwapper(target, norm);
  
  const start = +new Date();

  const initial = evaluateScore(stage);
  logScore(initial);

  let best = initial;

  const order = seq(length);
    
  const eps = 0.00001;

  const ds2 = new Float32Array(ds.length);

  fft3d(vs, fs, size);
  const mean = getMean(fs, size);

  lerpTarget(fs, size, mean, eps, target, norm);
  fft3d(fs, ds, size, { inverse: true });

  fft3d(vs, fs, size);
  diffTarget(fs, size, mean, eps, target, norm);
  fft3d(fs, ds2, size, { inverse: true });
  for (let i = 0; i < length; ++i) {
    const i2 = i * 2;
    ds2[i2] = ds2[i2];
    ds[i2] = ((ds[i2] - vs[i2]) / eps);
  }

  const [x, y, z] = point;
  
  const k1 = toIndex(x, y, z, size);
    
  const rows: any[] = [];
  const qs: any[] = [];
  
  console.log({k1, x, y, z, v: vs[k1*2]});

  //genRandom(stage, 100, 100);

  const tsv: any[] = ['k','x','y','z','q','d(c)/d(n)','ifft(d(c)/d(s))','v'];
  console.log(tsv.join('\t'));
  
  iterateIJK(size, (x, y, z, k2) => {
    
    swap(stage, k1, k2);
    const q = evaluateScore(stage);
    swap(stage, k1, k2);
    
    let d = best - q;
    
    qs.push(d);

    const v = vs[k2*2];
    const dcdn = ds[k2*2];
    const dcds = ds2[k2*2];

    rows.push([k2, x, y, z, d, dcdn, dcds, v]);

  });

  rows.sort((a, b) => qs[b[0]] - qs[a[0]]);

  console.log(rows.map(r => r.join("\t")).join("\n")); 
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('Start-up');

const defaults = {
  size: [64, 64, 16],

  point: [0, 0, 0],
  
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) : resolved.size;

const point = args.point ? parsePoint(args.point) : resolved.point;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, point, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, point);

logTime('Save');

saveAll(stage, true);

logTime('Done');
