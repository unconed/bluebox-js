import type { XYZ, Stage, Target, TypedArray } from '../lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, lerp, len2, iir, fmt } from '../lib/math';
import { logTime, logScore } from '../lib/log';
import {
  make2DTarget, make2DTTarget, getNorm,
  getMean, getScore, swapValues, getStdDev,
} from '../lib/target';
import {
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON, saveFreqMag,
} from '../lib/save';
import {
  allocStage,
  initEqual, checkHistogram, randomPoint,
  iterateIJK, iterateXYZ, iterateFXYZ, sampleXYZ,
  randomIndex, toIndex, fromIndex, wrapIndex,
  fft3d, binomialBlurValues,
} from '../lib/stage';
import { parseNumber, parsePoint, parseSize } from '../lib/parse';
import { genRandom } from '../lib/gen';

////////////////////////////////////////////////////////////////////////////////

const run = (
  stage: Stage,
  point: number[],
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const start = +new Date();

  for (let i = 0; i < length; ++i) {
    const i2 = i * 2;
    vs[i2] = 0;
    vs[i2+1] = 0;
  }
  
  vs[0] = 1;
  
  const bs1 = binomialBlurValues(vs, size, 0, false);
  const bs2 = binomialBlurValues(bs1, size, 1, false);
  const bs3 = binomialBlurValues(bs2, size, 0, false);
  const bs4 = binomialBlurValues(bs3, size, 1, false);
  const bs5 = binomialBlurValues(bs4, size, 0, false);
  const bs6 = binomialBlurValues(bs5, size, 1, false);
  const bs7 = binomialBlurValues(bs6, size, 0, false);
  const bs8 = binomialBlurValues(bs7, size, 1, false);
  const bs9 = binomialBlurValues(bs8, size, 0, false);
  const bs10 = binomialBlurValues(bs9, size, 1, false);
  const bs11 = binomialBlurValues(bs10, size, 0, false);
  const bs12 = binomialBlurValues(bs11, size, 1, false);
  const bs13 = binomialBlurValues(bs12, size, 0, false);
  const bs14 = binomialBlurValues(bs13, size, 1, false);

  fft3d(bs14, fs, size);

  iterateXYZ(size, (fx, fy, fz, i) => {
    const i2 = i * 2;
    fs[i2] = len2(fs[i2], fs[i2+1]);
    fs[i2+1] = 0;
  });

  saveFreqMag(stage, 1, 'blur-filter.png');
  
  const getTarget = target;

  iterateXYZ(size, (fx, fy, fz, i) => {
    const i2 = i * 2;
    fs[i2] = Math.sqrt(fs[i2] * getTarget(fx, fy, fz)) * 100;
   });
  
  saveFreqMag(stage, 1, 'blur-bandpass.png');
};

////////////////////////////////////////////////////////////////////////////////

logTime('Start-up');

const defaults = {
  size: [64, 64, 16],

  point: [0, 0, 0],
  
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) : resolved.size;

const point = args.point ? parsePoint(args.point) : resolved.point;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, point, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, point);
