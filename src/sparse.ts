import type { XYZ, Stage, Target, TypedArray } from './lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, len2 } from './lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from './lib/log';
import {
  make2DTarget, make2DTTarget, getNorm, getWindow, makeWindowedTarget,
  getMean, getScore, swapValues, diffTarget,
} from './lib/target';
import {
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON, saveState, saveFreqMag,
} from './lib/save';
import {
  allocStage,
  initEqual, checkHistogram,
  iterateIJK, iterateXYZ, iterateFXYZ,
  randomIndex, fromIndex, toIndex, wrapIndex,
  fft3d,
} from './lib/stage';
import { parseNumber, parsePoint, parseSize } from './lib/parse';

////////////////////////////////////////////////////////////////////////////////

const makeSparseSwapper = (
  subSize: number[],
  target: Target,
  norm: number,
  rangeTarget: Target,
  rangeNorm: number,
) => {

  const [rx, ry, rz] = subSize;
  const subLength = rx * ry * rz;
  const vss = new Float32Array(subLength * 2);

  const rx2 = rx/2;
  const ry2 = ry/2;
  const rz2 = rz/2;

  const nx = Math.ceil(rx2);
  const ny = Math.ceil(ry2);
  const nz = Math.ceil(rz2);

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    return evaluateBox(vs, fs, size, target, norm);
  };

  const evaluateAround = (xx: number, yy: number, zz: number, vs: TypedArray, fs: TypedArray, size: number[]) => {
    const [w, h, d] = size;

    let io = 0;
    for (let z = -Math.floor(rz2); z < rz2; ++z) {
      for (let y = -Math.floor(ry2); y < ry2; ++y) {
        for (let x = -Math.floor(rx2); x < rx2; ++x) {
          const wx = (xx + x + w) % w;
          const wy = (yy + y + h) % h;
          const wz = (zz + z + d) % d;

          const wnd = getWindow(x / nx, y / ny, z / nz);

          const k = toIndex(wx, wy, wz, size) * 2;
          vss[io]   = vs[k] * wnd;
          vss[io+1] = vs[k+1] * wnd;

          io += 2;
        }
      }
    }

    return evaluateBox(vss, fs, subSize, rangeTarget, rangeNorm);
  };

  const evaluateBox = (vs: TypedArray, fs: TypedArray, size: number[], target: Target, norm: number) => {
    fft3d(vs, fs, size);
    const mean = getMean(fs, size);
    return getScore(fs, size, mean, target, norm);
  };

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    const [x1, y1, z1] = fromIndex(k1, size);
    const [x2, y2, z2] = fromIndex(k2, size);

    const qa1 = evaluateAround(x1, y1, z1, vs, fs, size);
    const qb1 = evaluateAround(x2, y2, z2, vs, fs, size);

    swapValues(vs, k1, k2);

    const qa2 = evaluateAround(x1, y1, z1, vs, fs, size);
    const qb2 = evaluateAround(x2, y2, z2, vs, fs, size);

    const q1 = sqr(qa1) + sqr(qb1);
    const q2 = sqr(qa2) + sqr(qb2);

    if (q2 < q1) {
      return q2;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swapMaybe, evaluateScore};
}

const run = (
  stage: Stage,
  range: number,

  steps: number,
  checkpoint: number,
  limit: number,
  until: number,
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const r = range || 1;
  const rx = Math.min(w/2, r);
  const ry = Math.min(h/2, r);
  const rz = Math.min(d/2, r);
  const sub = [rx*2, ry*2, rz*2];

  const [rangeTarget, tvs, tfs] = makeWindowedTarget(sub, target);
  const rangeNorm = getNorm(rangeTarget, sub);

  saveState({...stage, vs: tvs, size: sub}, 'windowed-vs.png', false, -1, 1);
  saveFreqMag({...stage, fs: tfs, size: sub}, 1, 'windowed-fs.png');

  const {swapMaybe, evaluateScore} = makeSparseSwapper(sub, target, norm, rangeTarget, rangeNorm);

  const eps = 1/255;

  let success = 0;
  let failed = 0;
  let start = +new Date();

  const logRate = makeLogRate();
  const logStep = makeLogStep();

  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  let best = initial;

  for (let i = 0; i < steps; ++i) {

    if ((i % 64) == 0) {
      fft3d(vs, fs, size);

      const mean = getMean(fs, size);
      diffTarget(fs, size, mean, target, norm);

      fft3d(fs, ds, size, { inverse: true });
    }

    const k1 = randomIndex(size);
    const v1 = vs[k1*2];
    const dcdn1 = ds[k1*2];

    failed++;
    for (let j = 0; j < 64; ++j) {
      const k2 = randomIndex(size);
      const v2 = vs[k2*2];
      const dcdn2 = ds[k2*2];

      const dcdn = dcdn1 - dcdn2;
      const dg = (v1 - v2) * dcdn;
      if (dg <= 0) continue;

      const score = swapMaybe(stage, k1, k2, best);
      if (score != null) {
        best = score;

        logStep(i, best);

        success++;
        failed = 0;
        break;
      }
    }

    if (((i + 1) % checkpoint) == 0) {
      const score = evaluateScore(stage);
      logScore(score);
      logRate(score);
      saveAll(stage, false);
    }

    if (failed > limit) {
      console.warn('🛑 Iteration Aborted!');
      break;
    }

    if (until) {
      const now = +new Date();
      if (now - start > until) break;
    }
  }

  const final = evaluateScore(stage);
  logScore(final);
  logRate(final);

};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('== Sparse Scoring ==');

const defaults = {
  size: [64, 64, 16],
  steps: 100000,
  checkpoint: 10000,
  limit: 1000,
  until: 0,

  range: 8,
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) : resolved.size;
const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;
const until = args.until != null ? parseNumber(args.until) : resolved.until;

const range = parseNumber(args.range) || resolved.range;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, steps, checkpoint, limit, until, range, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, range, steps, checkpoint, limit, until);

logTime('Save');

saveAll(stage, true);

logTime('Done');
