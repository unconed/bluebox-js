import type { XYZ, Stage, Target, TypedArray } from './lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, lerp, len2, iir, fmt } from './lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from './lib/log';
import { makeSwapper, diffTarget, getMean } from './lib/target';  
import { 
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON,
} from './lib/save';
import {
  allocStage,
  initEqual, checkHistogram,
  iterateIJK, iterateXYZ, iterateFXYZ,
  randomIndex, toIndex, wrapIndex,
  fft3d, makeSampleOrder,
} from './lib/stage';
import { parseNumber, parsePoint, parseSize } from './lib/parse';

////////////////////////////////////////////////////////////////////////////////

const run = (
  stage: Stage,
  blue: Stage | null,
  samples: number,
  sparse: boolean,

  steps: number,
  checkpoint: number,
  limit: number,
  until: number,
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swap, swapMaybe, evaluateScore} = makeSwapper(target, norm);
  
  const start = +new Date();

  const logStep = makeLogStep();
  const logRate = makeLogRate();
  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  let best = initial;

  if (blue && stage.length !== blue.length) throw new Error("Accel size does not match stage");

  const [getOrderedPoint, updateOrder] = blue ? makeSampleOrder(blue) : [null, null];

  const percentSmoother = iir(0.05);
  const rateSmoother = iir(0.05);

  let success = 0;
  let failed = 0;
  
  let points: number[] = [];

  const order = seq(length);
  const swaps = [];
  
  let sample = 0; 

  for (let i = 0; i < steps; ++i) {

    let last = best;
    
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    diffTarget(fs, size, mean, target, norm);

    fft3d(fs, ds, size, { inverse: true });

    let swapped = 0;

    let tries = 0;
    let skips = 0;

    const n = samples;
    
    for (let j = 0; j < n; j++) {
      const k1 = randomIndex(size);
 
      const v1 = vs[k1*2];
      const dcdn1 = ds[k1*2];

      if (sparse) {
        tries++;

        let l = 0;
        for (let k = 0;; k++) {
          const k2 = getOrderedPoint ? getOrderedPoint(sample++) : randomIndex(size);
          const v2 = vs[k2*2];
          const dcdn2 = ds[k2*2];
          const dcdn = dcdn1 - dcdn2;

          const dg = (v1 - v2) * dcdn;
          if (dg <= 0 && l < 64) {
            l++;
            skips++;
            continue;
          }

          swap(stage, k1, k2);        
          swapped++;
          
          swaps.push(k1, k2);
          break;
        }
      }
      else {
        tries++;
        
        let l = 0;
        for (let k = 0;; k++) {
          const k2 = getOrderedPoint ? getOrderedPoint(sample++) : randomIndex(size);
          const v2 = vs[k2*2];
          const dcdn2 = ds[k2*2];
          const dcdn = dcdn1 - dcdn2;

          const dg = (v1 - v2) * dcdn;
          if (dg <= 0 && l < 64) {
            l++;
            skips++;
            continue;
          }
          
          swap(stage, k1, k2);        
          const score = evaluateScore(stage);

          if (score < best) {
            best = score;
            swapped++;
          }
          else {
            swap(stage, k1, k2);
          }
          break;
        }
      }
    }

    if (sparse) {
      const score = evaluateScore(stage);
      if (score < best) {
        best = score;
      }
      else {
        for (let i = swaps.length - 2; i >= 0; i -= 2) {
          swap(stage, swaps[i], swaps[i+1]);
        }
      }
      swaps.length = 0;
    }

    const r = swapped / Math.max(1, skips + swapped) * 100;
    const rs = rateSmoother(r);

    const p = sparse ? n / (skips + swapped) : swapped / n * 100;
    const ps = percentSmoother(p);

    if (swapped) {
      success++;
      failed = 0;

      if (sparse) {
        logStep(i, best, `(${n} / ${skips + swapped}) (~${(ps).toFixed(1)}%) (${(p).toFixed(1)}%) (!${fmt(rs)})`);
      } else {
        logStep(i, best, `(${swapped} / ${n}) (~${(ps).toFixed(1)}%) (${(p).toFixed(1)}%) (!${fmt(rs)})`);
      }
    }
    if (last < best) {
      failed++;
      if (true || (failed % 10) == 0) logStep(i, `⚠️ No convergence (${fmt(best)})`, '', true);
    }

    if (((i + 1) % checkpoint) == 0) {
      logScore(best);
      logRate(best);
      saveAll(stage, false);
    }

    if (failed > limit) {
      console.warn('🛑 Iteration Aborted!');
      break;
    }
    
    if (until) {
      const now = +new Date();
      if (now - start > until) break;
    }
  }
  
  const final = evaluateScore(stage);
  logScore(final);
  logRate(final);
  
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('== Gradient-based Selection ==');

const defaults = {
  size: [64, 64, 16],
  steps: 10,
  checkpoint: 3,
  limit: 1000,
  until: 0,

  samples: 128,
  sparse: false,
  
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) :resolved.size;
const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;
const until = args.until != null ? parseNumber(args.until) : resolved.until;

const samples = args.samples != null ? parseNumber(args.samples) : resolved.samples;
const sparse = !!args.sparse;

const accel = args.accel;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, steps, checkpoint, limit, until, samples, sparse, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

let blue = null;
if (accel) {
  blue = allocStage(size, space, time);
  loadState(blue, args.accel);
  logTime('Accel');
}

run(stage, blue, samples, sparse, steps, checkpoint, limit, until);

logTime('Save');

saveAll(stage, true);

logTime('Done');
