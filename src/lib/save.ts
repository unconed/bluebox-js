import type { XYZ, Stage, Target, Sampler, Iterator, IteratorFXYZ, TypedArray, FFTOptions } from './types';

import { copyFileSync, readFileSync, writeFileSync } from 'fs';
import { PNG } from 'pngjs';
import { fft3d, packDiffRGBA, packRealRGBA, unpackRealRGBA, packMagnitudeValues, packPhaseValues, packPhaseDiffRGBA, copyComplexArrayRange, shiftValues, binomialBlurValues, zBlurValues, errorValues } from './stage';
import { π } from './math';

export const copySnapshot = (a: string, b: string) => {
  copyFileSync(a, b);
};

export const saveSnapshot = (stage: Stage) => {
  const {size: [w, h, d]} = stage;
  
  saveDiff(stage, 'diff.png');
  savePhaseDiff(stage, 'diff-phase-1-2.png', 1/2, false);
  savePhaseDiff(stage, 'diff-phase-1-16.png', 1/16, false);
  savePhaseDiff(stage, 'diff-phase-1-256.png', 1/256, false);
  savePhaseDiff(stage, 'diff-phase-1-4096.png', 1/4096, true);
  saveState(stage, 'state.png');
  saveState(stage, 'time.png');
  saveState({...stage, size: [w, h, 1]}, 'time-1.png', true);
  saveState(stage, 'time-z.png', true);
  saveDebug(stage);
  
  console.log('✅ Saved');
};

export const saveDebug = (stage: Stage) => {
  const {vs, fs, size, target, norm, time} = stage;
  const [w, h, d] = size;

  fft3d(vs, fs, size);

  const es = errorValues(fs, size, target, norm);
  saveFreqMag({...stage, fs: es}, 1, 'error-xyz.png');

  saveFreqMag(stage, Math.sqrt(w*h*d), 'freq-xyz.png');
  saveFreqPhase(stage, 'phase-xyz.png');

  fft3d(vs, fs, size, {z: false});
  saveFreqMag(stage, Math.sqrt(w*h), 'freq-xy.png');
  saveFreqPhase(stage, 'phase-xy.png');

  fft3d(vs, fs, size, {x: false, y: false});
  saveFreqMag(stage, Math.sqrt(d), 'freq-z.png', true);
  saveFreqPhase(stage, 'phase-z.png', true);
  
  const bs1 = binomialBlurValues(vs,  size, 0, time);
  const bs2 = binomialBlurValues(bs1, size, 1, time);
  const bs3 = binomialBlurValues(bs2, size, 2, time);

  const bs4 = binomialBlurValues(bs3, size, 0, time);
  const bs5 = binomialBlurValues(bs4, size, 1, time);
  const bs6 = binomialBlurValues(bs5, size, 2, time);

  const bs = bs6;

  fft3d(bs, fs, size, {z: !time}) ;
  saveState({...stage, vs: bs}, !time ? 'blur-xyz.png' : 'blur-xy.png', false, -0.1, 0.1);
  saveFreqMag(stage, 1/40 * Math.sqrt(!time ? w*h*d : w*h), !time ? 'freq-blur-xyz.png' : 'freq-blur-xy.png');
  saveFreqPhase(stage, !time ? 'blur-phase-xyz.png' : 'blur-phase-xy.png');

  const zs = zBlurValues(vs, size);
  fft3d(zs, fs, size, {x: false, y: false});
  saveState({...stage, vs: zs}, 'blur-z.png', true);
  saveFreqMag(stage, 1/10 * Math.sqrt(d), 'freq-blur-z.png', true);
  saveFreqPhase(stage, 'blur-phase-z.png', true);
};


export const loadJSON = (file: string) => {
  try {
    const json = readFileSync(file).toString();
    const data = JSON.parse(json);
    return data;
  } catch (e) {
    return {};
  }
};

export const saveJSON = (data: any, file: string) => {
  const json = JSON.stringify(data, null, 2);
  writeFileSync(file, json);
};

export const loadState = (stage: Stage, file: string) => {
  const {vs, ls, ps, size} = stage;
  const [w, h, d] = size;

  try {
    const buffer = readFileSync(file);
    const png = PNG.sync.read(buffer);

    unpackRealRGBA(vs, png.data, size);
    copyComplexArrayRange(vs, ls, 0, 0, w*h*d, 1, 1);
    fft3d(vs, ps, size);
    
    return true;
  } catch (e) {
    return false;
  }
};

export const saveDiff = (stage: Stage, file: string) => {
  const {vs, ls, size, length} = stage;
  const [w, h, d] = size;

  const png = new PNG({
    width: w,
    height: h * d,
    colorType: 2,
    inputColorType: 2,
    bitDepth: 8,
  });

  packDiffRGBA(png.data, vs, ls, size, -1/32, 1/32);

  const buffer = PNG.sync.write(png);
  writeFileSync(file, buffer);
  
  const n2 = length * 2;
  for (let i = 0; i < n2; ++i) {
    ls[i] = vs[i];
  }
};

export const savePhaseDiff = (stage: Stage, file: string, norm: number, update: boolean = true) => {
  const {fs, ps, size, length} = stage;
  const [w, h, d] = size;

  const png = new PNG({
    width: w,
    height: h * d,
    colorType: 2,
    inputColorType: 2,
    bitDepth: 8,
  });

  let shifted1 = shiftValues(fs, size, true, false);
  let shifted2 = shiftValues(ps, size, true, false);

  packPhaseDiffRGBA(png.data, shifted1, shifted2, size, -π * norm, π * norm);

  const buffer = PNG.sync.write(png);
  writeFileSync(file, buffer);
  
  if (!update) return;

  const n2 = length * 2;
  for (let i = 0; i < n2; ++i) {
    ps[i] = fs[i];
  }
};

export const saveState = (stage: Stage, file: string, transpose: boolean = false, min: number = -1, max: number = 1) => {
  const {vs, size} = stage;
  const [w, h, d] = size;

  const png = new PNG({
    width: w,
    height: h * d,
    colorType: 2,
    inputColorType: 2,
    bitDepth: 8,
  });

  let shifted = shiftValues(vs, size, false, transpose);
  packRealRGBA(png.data, shifted, size, min, max);
  
  const buffer = PNG.sync.write(png);
  writeFileSync(file, buffer);
};

export const saveFreqMag = (stage: Stage, norm: number, file: string, transpose: boolean = false) => {
  const {fs, size} = stage;
  const [w, h, d] = size;

  const png = new PNG({
    width: w,
    height: h * d,
    colorType: 2,
    inputColorType: 2,
    bitDepth: 8,
  });

  let shifted = shiftValues(fs, size, true, transpose);

  packMagnitudeValues(png.data, shifted, size, 0, norm * 2);
  
  const buffer = PNG.sync.write(png);
  writeFileSync(file, buffer);
};

export const saveFreqPhase = (stage: Stage, file: string, transpose: boolean = false) => {
  const {fs, size} = stage;
  const [w, h, d] = size;

  const png = new PNG({
    width: w,
    height: h * d,
    colorType: 2,
    inputColorType: 2,
    bitDepth: 8,
  });

  let shifted = shiftValues(fs, size, true, transpose);

  packPhaseValues(png.data, shifted, size);
  
  const buffer = PNG.sync.write(png);
  writeFileSync(file, buffer);
};

