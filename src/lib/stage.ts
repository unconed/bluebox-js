import type { XYZ, Stage, Target, Sampler, Iterator, IteratorFXYZ, TypedArray, FFTOptions } from './types';

import FFT from 'fft.js';

import { make2DTarget, make2DTTarget, getMean, getNorm } from './target';
import { τ, smoothStep, clamp, seq, sqr, len2 } from './math';

export const allocStage = (
  size: number[],
  space: string,
  time: string,
  spaceRange: number[] = [0, 1],
  timeRange: number[] = [0, 1],
) => {
  const [w, h, d] = size;
  const length = w * h * d;
  
  const target = d > 1 ? make2DTTarget(space, time, spaceRange, timeRange) : make2DTarget(space, spaceRange);
  const norm = getNorm(target, size);

  return {
    size,
    length,
    target,
    norm,
    time: time !== 'none',

    vs: new Float32Array(length * 2),
    fs: new Float32Array(length * 2),
    ds: new Float32Array(length * 2),

    ls: new Float32Array(length * 2),
    ps: new Float32Array(length * 2),
  };
};

export const randomPoint = (size: number[]): XYZ => {
  const [w, h, d] = size;
  return [
    Math.floor(Math.random() * w),
    Math.floor(Math.random() * h),
    Math.floor(Math.random() * d),
  ];
};

export const randomIndex = (size: number[]): number => {
  const [w, h, d] = size;
  return toIndex(
    Math.floor(Math.random() * w),
    Math.floor(Math.random() * h),
    Math.floor(Math.random() * d),
    size,
  );
};

export const toIndex = (x: number, y: number, z: number, size: number[]) => {
  const [w, h, d] = size;
  return x + w * (y + h * z);
};

export const fromIndex = (i: number, size: number[]) => {
  const [w, h, d] = size;
  const x = i % w;
  const y = ((i - x) / w) % h;
  const z = (((i - x) / w) - y) / h;
  return [x, y, z];
};

export const wrapIndex = (x: number, y: number, z: number, size: number[]) => {
  const [w, h, d] = size;

  x = (x + w) % w;
  y = (y + h) % h;
  z = (z + d) % d;
  return toIndex(x, y, z, size);
};

export const errorValues = (fs: TypedArray, size: number[], target: Target, norm: number) => {
  const [w, h, d] = size;

  const mean = getMean(fs, size);

  const out = new Float32Array(fs.length);

  let v2 = 0;

  iterateXYZ(size, (fx, fy, fz, i) => {
    const i2 = i*2;
    const re = fs[i2];
    const im = fs[i2+1];
    const f = target(fx, fy, fz) * norm;
    
    const v = out[i*2] = len2(re, im) - f * mean;
    v2 += v*v;
  });

  const s = Math.sqrt(v2 / (w * h * d));
  iterateXYZ(size, (fx, fy, fz, i) => {
    out[i*2] /= s;
  });

  return out;
};

export const shiftValues = (vs: TypedArray, size: number[], shift: boolean, transpose: boolean) => {
  const [w, h, d] = size;
  const w2 = w / 2;
  const h2 = h / 2;
  const d2 = d / 2; 

  const out = new Float32Array(vs.length);
  
  const dx = shift ? Math.floor(w2) : 0;
  const dy = shift ? Math.floor(h2) : 0;
  const dz = shift ? Math.floor(d2) : 0;
  
  for (let z = 0; z < d; ++z) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {

        const k1 = x + w * (y + h * z);

        const k2 = transpose
          ? ((x + dx) % w) + w * (((z + dz) % d) + d * ((y + dy) % h))
          : ((x + dx) % w) + w * (((y + dy) % h) + h * ((z + dz) % d));

        out[k2*2]   = vs[k1*2];
        out[k2*2+1] = vs[k1*2+1];
      }
    }
  }

  return out;
};

export const binomialBlurValues = (vs: Float32Array, size: number[], axis: number, time: boolean) => {
  const [w, h, d] = size;

  const w1 = 1/4;
  const w2 = 1/2;
  const w3 = 1/4;

  const out = new Float32Array(vs.length);

  const getSample = (x: number, y: number, z: number) => vs[toIndex(
    (x + w) % w,
    (y + h) % h,
    (z + d) % d,
    size
  )*2];
  
  if (axis === 0 && w === 1) return vs;
  if (axis === 1 && h === 1) return vs;
  if (axis === 2 && (time || d === 1)) return vs;

  const getBlur = (
    axis === 0 ? (x: number, y: number, z: number) => (
      (w1 * getSample(x - 1, y, z) + w2 * getSample(x, y, z) + w3 * getSample(x + 1, y, z))
    ) :
    axis === 1 ? (x: number, y: number, z: number) => (
      (w1 * getSample(x, y - 1, z) + w2 * getSample(x, y, z) + w3 * getSample(x, y + 1, z))
    ) :
    /*axis === 2 ?*/ (x: number, y: number, z: number) => (
      (w1 * getSample(x, y, z - 1) + w2 * getSample(x, y, z) + w3 * getSample(x, y, z + 1))
    )
  );
  
  let io = 0;
  for (let z = 0; z < d; ++z) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {
        const v = getBlur(x, y, z);
        out[io]   = v;
        out[io+1] = 0.0;
        io += 2;
      }
    }
  }

  return out;
};

export const zBlurValues = (vs: TypedArray, size: number[]) => {
  const [w, h, d] = size;
  
  const samples: number = 9;

  const q = 0.75;

  const w1 = q*q*q*q*q*q*q*q;
  const w2 = q*q*q*q*q*q*q;
  const w3 = q*q*q*q*q*q;
  const w4 = q*q*q*q*q;
  const w5 = q*q*q*q;
  const w6 = q*q*q;
  const w7 = q*q;
  const w8 = q;
  const w9 = 1;

  const ws = 1/(w1+w2+w3+w4+w5+w6+w7+w8+w9);

  const out = new Float32Array(vs.length);

  const getSample = (x: number, y: number, z: number) => vs[toIndex(
    (x + w) % w,
    (y + h) % h,
    (z + d) % d,
    size
  )*2];

  const getBlur = (
    samples === 3 ? (
      (x: number, y: number, z: number) => ws * (
        w1 * getSample(x, y, z - 1) +
        w2 * getSample(x, y, z) +
        w3 * getSample(x, y, z + 1)
      )
    ) :
    samples === 5 ? (
      (x: number, y: number, z: number) => ws * (
        w1 * getSample(x, y, z - 2) +
        w2 * getSample(x, y, z - 1) +
        w3 * getSample(x, y, z) +
        w4 * getSample(x, y, z + 1) +
        w5 * getSample(x, y, z + 2)
      )
    ) :
    samples === 7 ? (
      (x: number, y: number, z: number) => ws * (
        w1 * getSample(x, y, z - 3) +
        w2 * getSample(x, y, z - 2) +
        w3 * getSample(x, y, z - 1) +
        w4 * getSample(x, y, z) +
        w5 * getSample(x, y, z + 1) +
        w6 * getSample(x, y, z + 2) +
        w7 * getSample(x, y, z + 3)
      )
    ) :
    samples === 9 ? (
      (x: number, y: number, z: number) => ws * (
        w1 * getSample(x, y, z - 4) +
        w2 * getSample(x, y, z - 3) +
        w3 * getSample(x, y, z - 2) +
        w4 * getSample(x, y, z - 1) +
        w5 * getSample(x, y, z) +
        w6 * getSample(x, y, z + 1) +
        w7 * getSample(x, y, z + 2) +
        w8 * getSample(x, y, z + 3) +
        w9 * getSample(x, y, z + 4)
      )
    ) :
    () => 0
  );

  let io = 0;
  for (let z = 0; z < d; ++z) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {
        const v = getBlur(x, y, z);
        out[io]   = v;
        out[io+1] = 0.0;
        io += 2;
      }
    }
  }

  return out;
};

export const packPhaseDiffRGBA = (
  out: Uint8Array,
  vs: TypedArray,
  ls: TypedArray,
  size: number[],
  min: number = -1,
  max: number = 1,
) => {
  const [w, h, d] = size;
  const n = w * h * d;

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const vl = Math.atan2(vs[ii+1], vs[ii]);
    const ll = Math.atan2(ls[ii+1], ls[ii]);

    const v = ((vl - ll) - min) / (max - min) % 1;

    const u = Math.round(v * 255);
    out[io  ] = u;
    out[io+1] = u;
    out[io+2] = u;
    out[io+3] = 255;

    io += 4;
    ii += 2;
  }
};

export const packDiffRGBA = (
  out: Uint8Array,
  vs: TypedArray,
  ls: TypedArray,
  size: number[],
  min: number = -1,
  max: number = 1,
) => {
  const [w, h, d] = size;
  const n = w * h * d;

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const v = clamp(((vs[ii] - ls[ii]) - min) / (max - min), 0, 1);

    const u = Math.round(v * 255);
    out[io  ] = u;
    out[io+1] = u;
    out[io+2] = u;
    out[io+3] = 255;

    io += 4;
    ii += 2;
  }
};

export const packRealRGBA = (
  out: Uint8Array,
  vs: TypedArray,
  size: number[],
  min: number = -1,
  max: number = 1,
) => {
  const [w, h, d] = size;
  const n = w * h * d;

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const v = clamp((vs[ii] - min) / (max - min), 0, 1);

    const u = Math.round(v * 255);
    out[io  ] = u;
    out[io+1] = u;
    out[io+2] = u;
    out[io+3] = 255;

    io += 4;
    ii += 2;
  }
};

export const unpackRealRGBA = (vs: TypedArray, inn: Uint8Array, size: number[]) => {
  const [w, h, d] = size;
  const n = w * h * d;

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const f = ((inn[ii]) / 255) * 2 - 1;
    vs[io] = f;

    io += 2;
    ii += 4;
  }
};

export const packMagnitudeValues = (out: Uint8Array, fs: TypedArray, size: number[], min: number = 0, max: number = 1) => {
  const [w, h, d] = size;
  const n = w * h * d;

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const m = Math.sqrt(sqr(fs[ii]) + sqr(fs[ii+1]));
    const u = clamp(Math.floor((m - min) / (max - min) * 255), 0, 255);

    out[io  ] = u;
    out[io+1] = u;
    out[io+2] = u;
    out[io+3] = 255;

    if (m == 0) {
      out[io+0] = 1;
      out[io+1] = 0;
      out[io+2] = 1;
    }

    io += 4;
    ii += 2;
  }
};

export const packPhaseValues = (out: Uint8Array, fs: TypedArray, size: number[]) => {
  const [w, h, d] = size;
  const n = w * h * d;

  const hat = (x: number) => clamp(smoothStep(Math.min(x, 2 - x)), 0, 1);
  const map = (x: number) => Math.round(clamp(x * 255, 0, 255));

  let io = 0;
  let ii = 0;
  for (let i = 0; i < n; ++i) {
    const m = Math.atan2(fs[ii+1], fs[ii]);
    const th = (((m % τ) + τ) % τ) / τ * 3;

    const q1 = Math.sqrt(hat(th));
    const q2 = Math.sqrt(hat((th + 1) % 3));
    const q3 = Math.sqrt(hat((th + 2) % 3));

    out[io  ] = map(q1);
    out[io+1] = map(q2);
    out[io+2] = map(q3);
    out[io+3] = 255;

    io += 4;
    ii += 2;
  }
};

export const initEqual = (stage: Stage) => {
  const {vs, ls, ps, size, size: [w, h, d], length} = stage;
  
  const equal = (size: number[], order: number[]) => (i: number) => {
    const [w, h, d] = size;
    const f = 256 / Math.min(256, w*h*d);
    const j = order[i];
    const x = (j * f + Math.floor(f / 2)) % 256;
    return x / 255 * 2 - 1;
  };

  const order = seq(length);
  const r = order.map(Math.random);
  order.sort((a, b) => r[a] - r[b]);

  fillReal(vs, equal(size, order));  
};

export const fillReal = (vs: TypedArray, f: (i: number) => number) => {
  const n = vs.length;
  for (let i = 0; i < n; ++i) {
    vs[i*2] = f(i);
    vs[i*2+1] = 0;
  }
};

export const copyComplexArrayRange = (
  from: TypedArray, to: TypedArray,
  fromIndex: number, toIndex: number, length: number,
  fromStride: number = 1, toStride: number = 1,
) => {
  const n = length;
  var fromBase = fromIndex * 2;
  var toBase = toIndex * 2;
  for (let i = 0; i < n; i++) {
    to[toBase    ] = from[fromBase    ];
    to[toBase + 1] = from[fromBase + 1];

    fromBase += 2 * fromStride;
    toBase += 2 * toStride;
  }
};

export const print = (vs: TypedArray, size: number[]) => {
  const [w, h, d] = size;

  const ww = Math.min(6, w);
  const hh = Math.min(6, h);
  const dd = Math.min(6, d);

  for (let z = 0; z < dd; ++z) {
    for (let y = 0; y < hh; ++y) {
      const row = [];
      for (let x = 0; x < ww; ++x) {
        let o = toIndex(x, y, z, size);
        row.push(vs[o * 2], vs[o * 2 + 1]);
      }
      console.log(row.map(x => x.toFixed(2)).join(', '));
    }
    console.log('\n');
  }
  console.log('\n\n');
}

const FFT_MAP = new Map<number, FFT>();
export const getFFT = (size: number) => {
  if (FFT_MAP.has(size)) return FFT_MAP.get(size)!;

  const fft = new FFT(size);
  FFT_MAP.set(size, fft);
  return fft;
}

const _vs = new Float32Array(1024);
const _fs = new Float32Array(1024);

export const fft3d = (vs: TypedArray, fs: TypedArray, size: number[], options?: FFTOptions) => {
  const [w, h, d] = size;
  const wh = w * h;
  const whd = wh * d;

  const xx = !!(options?.x ?? true);
  const yy = !!(options?.y ?? true);
  const zz = !!(options?.z ?? true);

  const fftX = getFFT(Math.max(2, w));
  const fftY = getFFT(Math.max(2, h));
  const fftZ = getFFT(Math.max(2, d));
  
  const op = options?.inverse ? 'inverseTransform' : 'transform';

  copyComplexArrayRange(vs, fs, 0, 0, whd, 1, 1);

  if (xx && w > 1) {
    for (let z = 0; z < d; ++z) {
      for (let y = 0; y < h; ++y) {
        let base = (z * h + y) * w;
        copyComplexArrayRange(fs, _vs, base, 0, w, 1, 1);
        fftX[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, w, 1, 1);
      }
    }
  }

  if (yy && h > 1) {
    for (let z = 0; z < d; ++z) {
      for (let x = 0; x < w; ++x) {
        let base = (z * h * w) + x;
        copyComplexArrayRange(fs, _vs, base, 0, h, w, 1);
        fftY[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, h, 1, w);
      }
    }
  }
  
  if (zz && d > 1) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {
        let base = (y * w) + x;
        copyComplexArrayRange(fs, _vs, base, 0, d, wh, 1);
        fftZ[op](_fs, _vs);
        copyComplexArrayRange(_fs, fs, 0, base, d, 1, wh);
      }
    }
  }
};

export const checkHistogram = (vs: TypedArray, size: number[], verbose: boolean = false) => {
  const [w, h, d] = size;
  const wh = w * h;
  const whd = wh * d;
  
  const histo = seq(256).map(_ => 0);

  let o = 0;
  for (let i = 0; i < whd; ++i) {
    histo[Math.round((vs[o] * .5 + .5) * 255)]++;
    o += 2;
  }
  
  const counts: any = {};
  for (let i = 0; i < 256; ++i) if (histo[i]) counts[histo[i]] = (counts[histo[i]]||0) + 1;

  const valid = Object.keys(counts).length === 1;
  if (!valid) {
    console.warn("❌ Histogram check failed!");
    console.warn(histo);
    console.warn(counts);
    process.exit();
    return false;
  }
  else {
    verbose && console.log('📊 Histogram valid');
    //verbose && console.info(histo);
    return true;
  }
}

export const iterateIJK = (size: number[], f: Iterator) => {
  const [w, h, d] = size;

  let i = 0;
  for (let z = 0; z < d; ++z) {
    for (let y = 0; y < h; ++y) {
      for (let x = 0; x < w; ++x) {
        f(x, y, z, i++);
      }
    }
  }    
};

export const iterateFXYZ = (size: number[], f: IteratorFXYZ) => {
  const [w, h, d] = size;

  const ix = 2/w; 
  const iy = 2/h; 
  const iz = 2/d; 

  let i = 0;
  for (let z = 0; z < d; ++z) {
    let dz = z*iz;
    let fz = Math.min(dz, 2 - dz);

    for (let y = 0; y < h; ++y) {
      let dy = y*iy;
      let fy = Math.min(dy, 2 - dy);

      for (let x = 0; x < w; ++x) {
        let dx = x*ix;
        let fx = Math.min(dx, 2 - dx);

        f(fx, fy, fz, x, y, z, i++);
      }
    }
  }    
};

export const iterateXYZ = (size: number[], f: Iterator) => {
  const [w, h, d] = size;

  const ix = 2/w; 
  const iy = 2/h; 
  const iz = 2/d; 

  let i = 0;
  for (let z = 0; z < d; ++z) {
    let dz = z*iz;
    let fz = Math.min(dz, 2 - dz);

    for (let y = 0; y < h; ++y) {
      let dy = y*iy;
      let fy = Math.min(dy, 2 - dy);

      for (let x = 0; x < w; ++x) {
        let dx = x*ix;
        let fx = Math.min(dx, 2 - dx);

        f(fx, fy, fz, i++);
      }
    }
  }    
};

export const sampleXYZ = <T>(x: number, y: number, z: number, size: number[], f: Sampler<T>) => {
  const [w, h, d] = size;

  const ix = 2/w; 
  const iy = 2/h; 
  const iz = 2/d; 

  let i = toIndex(x, y, z, size);

  let dz = z*iz;
  let fz = Math.min(dz, 2 - dz);

  let dy = y*iy;
  let fy = Math.min(dy, 2 - dy);

  let dx = x*ix;
  let fx = Math.min(dx, 2 - dx);

  return f(fx, fy, fz, i);
};

export const makeSampleOrder = (stage: Stage): [(i: number) => number, () => void] => {
  const {length, size} = stage;
  const [w, h, d] = size;

  const order = seq(length);
  
  const update = () => {
    const {vs} = stage;
    order.sort((a, b) => (vs[a*2] - vs[b*2]) || (a - b));
  };

  const sd = Math.floor(Math.random() * w * 4);
  const getOrderedPoint = (i: number) => {
    const o = Math.floor(i / length + sd);
    const [x, y, z] = fromIndex(i, size);
    const offset = wrapIndex((x + o)|0, (y + o/2)|0, (z + o/4)|0, size);
    return order[offset];
  };
  
  update();

  return [getOrderedPoint, update];
}
