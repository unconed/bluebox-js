import type { Stage, Target } from './types';
import { getMean, getScore, swapValues } from './target';
import { fft3d, randomIndex } from './stage';
import { iir } from './math';

export const makeSwapper = (
  target: Target,
  norm: number,
) => {

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    const q = getScore(fs, size, mean, target, norm);
    return q;
  };

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);

    const q = evaluateScore(stage);

    if (q < best) {
      return q;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swapMaybe, evaluateScore};
}

export const genRandom = (
  stage: Stage,
  steps: number,
  limit: number,
) => {
  const {vs, fs, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swapMaybe, evaluateScore} = makeSwapper(target, norm);

  const initial = evaluateScore(stage);

  let best = initial;
  let smoother = iir(0.25);
  let failed = 0;

  console.log('Generating random...');
  let checkpoint = Math.floor(steps / 10);

  for (let i = 0; i < steps; ++i) {

    const k1 = randomIndex(size);
    const k2 = randomIndex(size);

    const score = swapMaybe(stage, k1, k2, best);
    if (score != null) {
      best = score;
      failed = 0;
    }
    else {
      failed++;
    }

    if ((i % checkpoint) === 0) console.log(best);
    if (failed > limit) break;
  }

  console.log('Done.');

  const final = evaluateScore(stage);

  return {initial, final, failed};
};