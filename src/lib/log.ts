import { pad2, fmt, fmtTime, iir, sqr, lerp, smoothStep } from './math';

const start = +new Date();
export const logTime = (s: string) => {
  const ms = +new Date() - start;
  console.log(`[${ms}ms] ${s}`);
};

export const logScore = (score: number) => {
  console.log('📈 =', score);
};

export const makeLogStep = () => {
  let lastLog = 0;
  let lastStep = 0;
  
  const stepSmoother = iir(0.25);
  
  let silent = 0;
  return (i: number, best: string | number, extra: string = '', force: boolean = false) => {
    const now = +new Date();
    const dt = now - lastLog;

    if (force || (dt > 2000)) {
      let stepsPS = (i - lastStep) / dt * 1000;

      console.log('step', i, '<', typeof best === 'number' ? fmt(best, 6) : best, silent ? `[+${silent}]` : '', extra, lastStep ? stepSmoother(stepsPS) : '');
      lastLog = now;
      lastStep = i;
      silent = 0;
      return true;
    }
    else {
      silent++;
      return false;
    }
  };
};

export const makeLogRate = (k: number = 0.125, samples: number = Math.ceil(1/k)) => {

  let lastVals: number[] = [];
  let lastTime = start;
  let lastBest = 0;

  const rateSmoother = iir(0.25);

  return (best: number) => {
    const end = +new Date() / 1000;

    if (lastBest === 0) {
      lastTime = end;
      lastBest = best;
      return;
    }
    const dt = end - lastTime;

    const instant = (lastBest - best) / dt;
    const rate = rateSmoother(instant);

    const x = 0;
    const y = best;

    if (rate < 0) {
      console.log(`⚠️  No convergence - ${rate}`);
    }

    const est = rate ? Math.floor(best / rate) : 0;
    console.log('⏱️  =', fmt(rate), `(${fmt(instant)})`, fmtTime(est));

    lastTime = end;
    lastBest = best;
  };
};
