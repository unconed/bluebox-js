import type { Stage, TypedArray, Target } from './types';

import { π, sqr, lerp, len2, len3, clamp, smoothStep } from './math';
import { fft3d, toIndex, fromIndex, iterateXYZ, iterateIJK } from './stage';

const NORM: string = 'E1';

const cos = (x: number) => Math.cos(x);
const abs = (x: number) => Math.abs(x);
const select = (a: number, b: number, c: number | boolean) => c ? b : a;

export const getMean = (
  fs: TypedArray,
  size: number[],
) => {
  const [w, h, d] = size;
  const whd = w * h * d;

  let mean = 0;
  iterateIJK(size, (x, y, z, i) => {
    const i2 = i * 2;
    const re = fs[i2];
    const im = fs[i2+1];
    const l = len2(re, im);

    if (NORM === 'E2') mean += l*l;
    else mean += l;
  });
  mean /= whd;
  return mean;
};

export const getStdDev = (
  fs: TypedArray,
  mean: number,
  size: number[],
) => {
  const [w, h, d] = size;
  const whd = w * h * d;

  let x2 = 0;
  iterateIJK(size, (x, y, z, i) => {
    const i2 = i * 2;
    const re = fs[i2];
    const im = fs[i2+1];
    const l = len2(re, im);

    x2 += l*l;
  });

  let s = x2 / whd - mean * mean;
  return Math.sqrt(s);
};

export const getNorm = (target: Target, size: number[]) => {
  const [w, h, d] = size;
  let p = 0;
  if (NORM === 'E2') {
    iterateXYZ(size, (fx, fy, fz) => {
      p += sqr(target(fx, fy, fz));
    });
  }
  else {
    let i = 0;
    iterateXYZ(size, (fx, fy, fz) => {
      p += target(fx, fy, fz);
    });
  }
  return w * h * d / p;
};

export const getScore = (
  fs: TypedArray,
  size: number[],
  mean: number,
  target: Target,
  norm: number,
): number => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  if (NORM === 'E2') {
    iterateXYZ(size, (fx, fy, fz, i) => {
      let i2 = i*2;
      let re = fs[i2];
      let im = fs[i2+1];
      let f = target(fx, fy, fz);
      let l = len2(re, im);
      q += sqr(sqr(l) / mean - sqr(f) * norm);
    });
  }
  else {
    iterateXYZ(size, (fx, fy, fz, i) => {
      let i2 = i*2;
      let re = fs[i2];
      let im = fs[i2+1];
      let f = target(fx, fy, fz);
      let l = len2(re, im);
      q += sqr(l / mean - f * norm);
    });
  }

  return Math.sqrt(q / length);
};

export const getError = (
  fs: TypedArray,
  size: number[],
  mean: number,
  target: Target,
  norm: number,
): number => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let f = target(fx, fy, fz) * norm;
    q += sqr(len2(re, im) / mean - f);
  });

  return Math.sqrt(q / length);
};

export const swapValues = (vs: TypedArray, k1: number, k2: number) => {
  const i1 = k1 * 2;
  const i2 = k2 * 2;
  const v1 = vs[i1];
  const v2 = vs[i2];
  vs[i1] = v2;
  vs[i2] = v1;
};

const getLinear = (t: number, r: number[]): number => {
  const [a, b] = r;
  return clamp((t - a) / (b - a), 0, 1);
}

const getCosine = (t: number, r: number[]): number => {
  let f = getLinear(t, r);
  let q = .5 - .5 * cos(f * π);
  return q;
}

const getCosineQ = (t: number, r: number[]): number => {
  let f = getLinear(t, r);
  let q1 = .5 - .5 * cos(f * π);
  let q2 = .5 - .5 * cos(q1 * π);
  return q2;
}

const getComplement = (t: number, r: number[]): number => {
  let f = getLinear(t, r);
  let c = cos(f * π / 2);
  let c2 = c*c;
  let c4 = c2*c2;
  let c8 = c4*c4;

  let s = clamp(1 - 2.0 * c8, 0, 1);
  let s2 = s*s;
  let s4 = s2*s2;
  let s8 = s4*s4;

  return s8;
};

const makeTarget = (curve: string) => {
  if (curve === 'linear') return getLinear;
  if (curve === 'cosine') return getCosine;
  if (curve === 'cosineQ') return getCosineQ;
  if (curve === 'complement') return getComplement;

  return getLinear;
}

export const make2DTTarget = (space: string, time: string, spaceRange: number[], timeRange: number[]) => {
  if (time === 'none') {
    const getXYZTarget = makeTarget(space);
    return (fx: number, fy: number, fz: number) => {
      return getXYZTarget(len3(fx, fy, fz), spaceRange);
    }
  }
  else {
    const getXYTarget = makeTarget(space);
    const getZTarget = makeTarget(time);
    return (fx: number, fy: number, fz: number) => {
      return getXYTarget(len2(fx, fy), spaceRange) * getZTarget(fz, timeRange);
    };
  }
};

export const make2DTarget = (curve: string, tweak: number[]) => {
  const getTarget = makeTarget(curve);
  return (fx: number, fy: number, fz: number) => {
    return getTarget(len2(fx, fy), tweak);
  }
};

export const getWindow = (fx: number, fy: number, fz: number) => {
  const dr = Math.min(1, Math.sqrt(sqr(fx) + sqr(fy) + sqr(fz)));
  const hr = 1 - smoothStep(dr);
  return hr;
}

export const makeWindowedTarget = (size: number[], target: Target): [
  (fx: number, fy: number, fz: number) => number,
  Float32Array,
  Float32Array,
] => {
  const [w, h, d] = size;
  const whd = w * h * d;

  const ws = new Float32Array(whd * 2);
  const vs = new Float32Array(whd * 2);
  const fs = new Float32Array(whd * 2);

  const rx2 = w/2;
  const ry2 = h/2;
  const rz2 = d/2;

  const nx = Math.ceil(rx2);
  const ny = Math.ceil(ry2);
  const nz = Math.ceil(rz2);

  let io = 0;
  for (let z = -Math.floor(rz2); z < rz2; ++z) {
    for (let y = -Math.floor(ry2); y < ry2; ++y) {
      for (let x = -Math.floor(rx2); x < rx2; ++x) {
        const wx = (x + w) % w;
        const wy = (y + h) % h;
        const wz = (z + d) % d;

        // Windowing seems not worth it?
        const wnd = getWindow(x / nx, y / ny, z / nz);

        ws[io]   = wnd;
        ws[io+1] = 0;

        io += 2;
      }
    }
  }

  fft3d(ws, fs, size);

  iterateXYZ(size, (fx, fy, fz, i) => {
    i *= 2;
    const re = fs[i];
    const im = fs[i+1];

    fs[i] = len2(re, im);
    fs[i+1] = 0;
  });

  fft3d(fs, ws, size, {inverse: true});

  iterateXYZ(size, (fx, fy, fz, i) => {
    i *= 2;
    fs[i] = target(fx, fy, fz);
    fs[i+1] = 0;
  });

  //console.log('>', fs[10], fs[12], fs[14])

  fft3d(fs, vs, size, {inverse: true});

  iterateXYZ(size, (fx, fy, fz, i) => {
    i *= 2;
    vs[i] *= ws[i]
    vs[i+1] *= ws[i+1];
  });

  fft3d(vs, fs, size);
  //console.log('>', fs[10], fs[12], fs[14])

  iterateXYZ(size, (fx, fy, fz, i) => {
    i *= 2;
    const re = fs[i];
    const im = fs[i+1];

    fs[i] = Math.sqrt(sqr(re) + sqr(im));
    fs[i+1] = 0;
  });

  //console.log('>', fs[10], fs[12], fs[14])

  const f = (fx: number, fy: number, fz: number) => {
    const x = Math.floor(fx * w/2);
    const y = Math.floor(fy * h/2);
    const z = Math.floor(fz * d/2);
    const k = toIndex(x, y, z, size);
    return fs[k*2];
  };

  return [f, vs, fs];
};

export const lerpTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = lerp(l, f * mean, k) / l;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

export const diffTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l ? (l - f * mean) / l : 0;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

export const makeSwapper = (
  target: Target,
  norm: number,
) => {

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    const q = getScore(fs, size, mean, target, norm);
    return q;
  };

  const swap = (
    stage: Stage,
    k1: number,
    k2: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);
  }

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);

    const q = evaluateScore(stage);

    if (q < best) {
      return q;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swap, swapMaybe, evaluateScore};
};

