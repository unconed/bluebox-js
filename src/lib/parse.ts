export const parseNumber = (s: string) => +s || 0;
export const parseSize = (s: string) => {
  const parts = s.split(/[,x]/g).map(s => +s || 1);
  return [parts[0] || 1, parts[1] || 1, parts[2] || 1];
};

export const parsePoint = (s: string) => {
  const parts = s.split(/[,x]/g).map(s => +s || 0);
  return [parts[0] || 0, parts[1] || 0, parts[2] || 0];
};
