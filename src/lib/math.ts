export const π = Math.PI;
export const τ = 2 * π;

export const lerp = (a: number, b: number, t: number) => a * (1 - t) + b * t;

export const clamp = (x: number, a: number, b: number) => Math.max(a, Math.min(b, x));

export const seq = (n: number, s: number = 0, d: number = 1) => Array.from({ length: n }).map((_, i: number) => s + d * i);

export const smoothStep = (x: number) => (3 - 2 * x) * x * x;

export const sqr = (x: number) => x * x;

export const len2 = (x: number, y: number) => Math.sqrt(sqr(x) + sqr(y));
export const len3 = (x: number, y: number, z: number) => Math.sqrt(sqr(x) + sqr(y) + sqr(z));

export const fmt = (x: number, s: number = 2) => {
  const d = x ? Math.max(1, Math.min(12, Math.ceil(-Math.log10(Math.abs(x))) + s)) : 1;
  return x.toFixed(d);
}

export const fmtTime = (time: number) => {
  const d = Math.floor((time) / 86400);
  const h = Math.floor((time - d * 86400) / 3600);
  const m = Math.floor((time - d * 86400 - h * 3600) / 60);
  const s = Math.floor((time - d * 86400 - h * 3600 - m * 60));
  return `${pad2(d)}:${pad2(h)}:${pad2(m)}:${pad2(s)}`;
}

export const pad2 = (x: number) => ('00' + x).slice(-2);

export const iir = (k: number = 0.25, x: number = 0) => {
  let samples = Math.ceil(1/k);
  let int = 0;
  let avg = 0;
  let c = 0;

  return (x: number) => {
    if (c < samples) {
      int = avg = ((avg * c) + x) / (c + 1);
    }
    else {
      int = lerp(int, x, k);
      avg = lerp(avg, int, k);
    }
    c++;

    return avg;
  };
};
