export type XYZ = [number, number, number];

export type FFTOptions = {
  x?: boolean,
  y?: boolean,
  z?: boolean,
  inverse?: boolean,
};

export type TypedArray =
  Int8Array |
  Uint8Array |
  Int16Array |
  Uint16Array |
  Int32Array |
  Uint32Array |
  Uint8ClampedArray |
  Float32Array |
  Float64Array;

export type Sampler<T> = (x: number, y: number, z: number, i: number) => T;
export type Iterator = (x: number, y: number, z: number, i: number) => void;
export type IteratorFXYZ = (fx: number, fy: number, fz: number, x: number, y: number, z: number, i: number) => void;

export type Target = (fx: number, fy: number, fz: number) => number;

export type Stage = {
  size: number[],
  length: number,
  
  target: Target,
  norm: number,
  time: boolean,

  vs: Float32Array,
  fs: Float32Array,
  ds: Float32Array,

  ls: Float32Array,
  ps: Float32Array,
};