import type { XYZ, Stage, Target, TypedArray } from './lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, len2 } from './lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from './lib/log';
import { makeSwapper } from './lib/target';
import { 
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON,
} from './lib/save';
import {
  allocStage,
  initEqual, checkHistogram,
  iterateIJK, iterateXYZ, iterateFXYZ,
  randomIndex, toIndex, wrapIndex,
  fft3d,
} from './lib/stage';
import { parseNumber, parsePoint, parseSize } from './lib/parse';

////////////////////////////////////////////////////////////////////////////////

const run = (
  stage: Stage,
  block: number,

  steps: number,
  checkpoint: number,
  limit: number,
) => {
  const {vs, fs, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swapMaybe, evaluateScore} = makeSwapper(target, norm);
  
  let success = 0;
  let failed = 0;
  let start = +new Date();

  const logRate = makeLogRate();
  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  const bw = Math.min(block, w);
  const bh = Math.min(block, h);
  const bd = Math.min(block, d);

  const bw2 = Math.ceil(bw / 2);
  const bh2 = Math.ceil(bw / 2);
  const bd2 = Math.ceil(bw / 2);

  const gw = Math.max(1, Math.floor(w / bw));
  const gh = Math.max(1, Math.floor(h / bh));
  const gd = Math.max(1, Math.floor(d / bd));

  const blockSize = [bw, bh, bd];
  const blockStage = allocStage(blockSize);

  const {vs: bvs, fs: bfs} = blockStage;
  const solo = !(gw > 1 || gh > 1 || gd >> 1);

  let bi = 0;
  const bn = solo ? 1 : gw * gh * gd * 2;
  
  const runBlock = (ox: number, oy: number, oz: number) => {

    const logBlockStep = makeLogStep();
    const logBlockRate = makeLogRate();

    iterateIJK(blockSize, (x, y, z, i) => {
      const k = wrapIndex(ox + x, oy + y, oz + z, size);
      bvs[i*2] = vs[k*2];
    });

    let best = evaluateScore(blockStage);
    logScore(best);
    //logBlockRate(best);

    for (let i = 0; i < steps; ++i) {

      const k1 = randomIndex(blockSize);
      const k2 = randomIndex(blockSize);

      const score = swapMaybe(blockStage, k1, k2, best);
      if (score != null) {
        best = score;

        logStep(i, best);

        success++;
        failed = 0;
      }
      else {
        failed++;
      }

      if (((i + 1) % checkpoint) == 0) {
        logScore(best);
        if (solo) logBlockRate(best);
        if (solo) {
          iterateIJK(blockSize, (x, y, z, i) => {
            const k = wrapIndex(ox + x, oy + y, oz + z, size);
            vs[k*2] = bvs[i*2];
          });
          saveAll(stage, false);
        }
      }

      if (failed > limit) {
        console.warn('🛑 Iteration Aborted!');
        break;
      }
    }

    iterateIJK(blockSize, (x, y, z, i) => {
      const k = wrapIndex(ox + x, oy + y, oz + z, size);
      vs[k*2] = bvs[i*2];
    });

    //logBlockRate(best);

    const score = evaluateScore(stage);
    logScore(score);
    logRate(score);
  };
  
  for (let bz = 0; bz < gd; ++bz) {
    for (let by = 0; by < gh; ++by) {
      for (let bx = 0; bx < gw; ++bx) {
        bi++;
        
        const ox = bx * bw;
        const oy = by * bh;
        const oz = bz * bd;
        
        console.log(`Block (${bi} / ${bn}): (${ox}, ${oy}, ${oz}) - (${ox + bw}, ${oy + bh}, ${oz + bd})`);

        runBlock(ox, oy, oz);
      }
    }
  }

  if (!solo) for (let bz = 0; bz < gd; ++bz) {
    for (let by = 0; by < gh; ++by) {
      for (let bx = 0; bx < gw; ++bx) {
        bi++;

        const ox = bx * bw + bw2;
        const oy = by * bh + bh2;
        const oz = bz * bd + bd2;
        
        console.log(`⬜️ Block (${bi} / ${bn}): (${ox}, ${oy}, ${oz}) - (${ox + bw}, ${oy + bh}, ${oz + bd})`);

        runBlock(ox, oy, oz);
      }
    }
  }
  
  const final = evaluateScore(stage);
  logScore(final);
  logRate(final);
  
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('== Chunked Processing ==');

const defaults = {
  size: [64, 64, 16],
  steps: 1000,
  checkpoint: 1e10,
  limit: 1000,

  block: 6,
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) : resolved.size;
const steps = parseNumber(args.steps) ?? resolved.steps;
const checkpoint = parseNumber(args.checkpoint) || resolved.checkpoint;
const limit = parseNumber(args.limit) || resolved.limit;

const block = parseNumber(args.block) || resolved.block;

const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;

log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, block, steps, checkpoint, limit);

logTime('Save');

saveAll(stage, true);

logTime('Done');
