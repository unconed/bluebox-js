import type { XYZ, Stage, Target, TypedArray } from './lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, len2, iir } from './lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from './lib/log';
import { makeSwapper } from './lib/target';  
import { 
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON,
} from './lib/save';
import {
  allocStage,
  initEqual, checkHistogram,
  iterateIJK, iterateXYZ, iterateFXYZ,
  randomIndex, toIndex, wrapIndex,
  fft3d,
} from './lib/stage';
import { parseNumber, parsePoint, parseSize } from './lib/parse';

////////////////////////////////////////////////////////////////////////////////

const run = (
  stage: Stage,

  steps: number,
  checkpoint: number,
  limit: number,
  until: number,
  accept: number,
) => {
  const {vs, fs, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swap, swapMaybe, evaluateScore} = makeSwapper(target, norm);

  let success = 0;
  let pairs = 0;
  let failed = 0;
  
  let start = +new Date();

  const logRate = makeLogRate();
  const logStep = makeLogStep();

  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  let best = initial;

  let smoother = iir(0.05);

  let i;
  for (i = 0; i < steps; ++i) {

    const k1 = randomIndex(size);
    const k2 = randomIndex(size);
    pairs++;

    const score = swapMaybe(stage, k1, k2, best);
    if (score != null) {
      best = score || 0;

      const p = (success / pairs) * 100;
      const s = smoother(p);

      if (logStep(i, best, `(${success} / ${pairs}) (~${(s).toFixed(1)}%) (${(p).toFixed(1)}%)`)) {
        success = 0;
        pairs = 0;
      }

      success++;
      failed = 0;
    }
    else {
      failed++;
    }

    if (((i + 1) % checkpoint) == 0) {
      logScore(best);
      logRate(best);
      saveAll(stage, false);
    }

    if (failed > limit) {
      console.warn('🛑 Iteration Aborted!');
      break;
    }

    if (accept && success >= accept) break;

    if (until) {
      const now = +new Date();
      if (now - start > until) break;
    }
  }

  const p = (success / pairs) * 100;
  const s = smoother(p);
  logStep(i, best, `(${success} / ${pairs}) (~${(s).toFixed(1)}%) (${(p).toFixed(1)}%)`, true);

  const final = evaluateScore(stage);
  logScore(final);
  logRate(final);
  
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('== Random Selection ==');

const defaults = {
  size: [64, 64, 16],
  steps: 100000,
  checkpoint: 1000,
  limit: 10000,
  until: 0,
  accept: 0,

  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) : resolved.size;
const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;
const until = args.until != null ? parseNumber(args.until) : resolved.until;
const accept = args.accept != null ? parseNumber(args.accept) : resolved.accept;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, steps, checkpoint, limit, until, accept, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, steps, checkpoint, limit, until, accept);

logTime('Save');

saveAll(stage, true);

logTime('Done');
