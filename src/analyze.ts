import type { XYZ, Stage, Target, TypedArray } from './lib/types';

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { π, τ, seq, sqr, lerp, len2, iir, fmt } from './lib/math';
import { logTime, logScore, makeLogRate, makeLogStep } from './lib/log';
import {
  make2DTarget, make2DTTarget, getNorm,
  getMean, getScore, swapValues, getStdDev,
} from './lib/target';
import { 
  loadState, saveSnapshot, copySnapshot, loadJSON, saveJSON, saveState,
} from './lib/save';
import {
  allocStage,
  initEqual, checkHistogram, randomPoint,
  iterateIJK, iterateXYZ, iterateFXYZ, sampleXYZ,
  randomIndex, toIndex, fromIndex, wrapIndex,
  fft3d,
} from './lib/stage';
import { parseNumber, parsePoint, parseSize } from './lib/parse';

////////////////////////////////////////////////////////////////////////////////

const lerpTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l ? lerp(l, f * mean, k) / l : 0;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const diffTarget = (
  fs: TypedArray,
  size: number[],
  mean: number,
  k: number,
  target: Target,
  norm: number,
) => {
  const [w, h, d] = size;
  const length = w * h * d;

  let q = 0;
  iterateXYZ(size, (fx, fy, fz, i) => {
    let i2 = i*2;
    let re = fs[i2];
    let im = fs[i2+1];
    let l = len2(re, im);

    let f = target(fx, fy, fz) * norm;
    let r = l / mean - f;

    fs[i2] = re * r;
    fs[i2+1] = im * r;
  });

};

const makeSwapper = (
  target: Target,
  norm: number,
) => {

  const evaluateScore = (stage: Stage) => {
    const {vs, fs, size} = stage;
    fft3d(vs, fs, size);

    const mean = getMean(fs, size);
    const q = getScore(fs, size, mean, target, norm);
    return q;
  };

  const swap = (
    stage: Stage,
    k1: number,
    k2: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);
  }

  const swapMaybe = (
    stage: Stage,
    k1: number,
    k2: number,
    best: number,
  ) => {
    const {vs, fs, size} = stage;

    swapValues(vs, k1, k2);

    const q = evaluateScore(stage);

    if (q < best) {
      return q;
    }
    else {
      swapValues(vs, k1, k2);
      return null;
    }
  };

  return {swap, swapMaybe, evaluateScore};
}

const run = (
  stage: Stage,
  samples: number,
  retry: number,
  focus: number,

  steps: number,
  checkpoint: number,
  limit: number,
) => {
  const {vs, fs, ds, size, length, target, norm} = stage;
  const [w, h, d] = size;

  const {swap, evaluateScore} = makeSwapper(target, norm);
  
  const start = +new Date();

  const logStep = makeLogStep();
  const logRate = makeLogRate();
  const initial = evaluateScore(stage);
  logScore(initial);
  logRate(initial);

  let best = initial;

  const order = seq(length);
    
  const eps = 0.00001;

  let smoother = iir(0.25);

  let success = 0;
  let failed = 0;
  
  let duds = new Set<number>();
  let peaks: number[] = [];
  
  let points: number[] = [];

  const getBiasedPoint = (flip: boolean = false) => () => {
    const i = Math.floor(Math.pow(Math.random(), 4) * order.length);
    return order[flip ? order.length - 1 - i : i];
  };
  
  const getRandomPoint = () => () => randomIndex(size);
  const getPeak = () => (i: number) => order[i];
  const getNoPeak = () => (i: number) => order[order.length - 1 - i];

  const ds2 = new Float32Array(ds.length);

  const tsv: any[] = ['x','y','z','min','max','avg','q','d(c)/d(n)','ifft(d(c)/d(s))'];
  console.log(tsv.join('\t'));

  steps = 1;
  for (let i = 0; i < steps; ++i) {

    fft3d(vs, fs, size);
    const mean = getMean(fs, size);

    lerpTarget(fs, size, mean, eps, target, norm);
    fft3d(fs, ds, size, { inverse: true });

    fft3d(vs, fs, size);
    diffTarget(fs, size, mean, eps, target, norm);
    fft3d(fs, ds2, size, { inverse: true });
    for (let i = 0; i < length; ++i) {
      const i2 = i * 2;
      ds2[i2] = ds2[i2];
      ds[i2] = ((ds[i2] - vs[i2]) / eps);
    }

    for (let i = 0; i < samples; ++i) {
      let k1 = randomIndex(size);
      let [x, y, z] = fromIndex(k1, size);
      
      let better = 0;
      let worse = 0;

      let min = 0;
      let avg = 0;
      let max = 0;
      let count = 0;
      
      iterateIJK(size, (x, y, z, k2) => {
        
        swap(stage, k1, k2);
        const q = evaluateScore(stage);
        swap(stage, k1, k2);
        
        if (q < best) better++;
        else worse++;
      
        let d = best - q;
        min = Math.min(d, min);
        max = Math.max(d, max);
        avg += d;
        count++;
      });

      avg /= count;
      
      const q = better / (better + worse);
      const dcdn = ds[k1*2];
      const dcds = ds2[k1*2];
      
      console.log([x, y, z, min, max, avg, q, dcdn, dcds].join('\t'));
    }
    /*

    order.sort((a, b) => ds[b*2] - ds[a*2]);

    saveState({...stage, vs: ds, size}, 'delta.png', false, -1, 1);

    let N = 1024*32;

    const run = (name: string, f1: (i: number) => number, f2: (i: number) => number) => {
      const score = evaluateScore(stage);

      let good = 0;
      let accum = 0;
      for (let i = 0; i < N; ++i) {
        const k1 = f1(i);
        const k2 = f2(i);

        swap(stage, k1, k2);
        const q = evaluateScore(stage);
        swap(stage, k1, k2);

        if (q < score) {
          good++;
          accum += score - q;
        }
      }
      const percent = 100 * good / N;
      const avg = accum / good;
      console.log(name, `${good} / ${N} @ ${fmt(avg * good)} / ${fmt(avg * N)} (${percent.toFixed(1)}%)`);
    }
    
    //console.log(order.slice(0, 32).map((i) => ds[i*2]));
    
    run('peaks', getPeak(), getRandomPoint());
    run('peaks2', getPeak(), getNoPeak());
    run('random', getRandomPoint(), getRandomPoint());
    run('biased1', getBiasedPoint(), getRandomPoint());
    run('biased2', getBiasedPoint(), getBiasedPoint());
    run('biasedO', getBiasedPoint(), getBiasedPoint(true));



    focus = order.length;
    fft3d(vs, fs, size);

    let bestF = -1;
    let bestI = 0;
    let bestL = [0, 0];
    for (let k = 0; k < Math.max(1, focus); ++k) {

      const index = k;//randomIndex(size);
      const [mx, my, mz] = fromIndex(index, size);
    
      const [df, dl] = sampleXYZ(mx, my, mz, size, (fx, fy, fz, i) => {
        let i2 = i*2;
        let re = fs[i2];
        let im = fs[i2+1];
        let l = len2(re, im);

        let f = target(fx, fy, fz) * norm;
        let e = l - f;

        return [e, [l, f]];
      });
      
      const dfa = Math.abs(df);
      if (dfa > bestF) {
        bestF = df;
        bestL = dl;
        bestI = index;
      }
    }
    console.log(bestF, bestI, bestL)
    
    const i2 = bestI;
    let re = fs[i2];
    let im = fs[i2+1];

    const [mx, my, mz] = fromIndex(bestI, size);
    const amp = bestF;
    const phase = Math.atan2(re, im);

    const ups: number[] = [];
    const downs: number[] = [];
    
    /*
    const r = (w*h*d);
    dfs[bestI*2] = Math.cos(phase) * r;
    dfs[bestI*2+1] = Math.sin(phase) * r;

    fft3d(dfs, dvs, size, { inverse: true });
    */    

    /*
    let pos = 0;
    let neg = 0;
    let i = 0;
    while (Math.min(pos, neg) < N && i < N*16) {
      const k = randomIndex(size);
      const [x, y, z] = fromIndex(k, size);

      const th = (mx * x / w + my * y / w + mz * z / w) * τ;
      const dp = Math.cos(th + phase) * amp;

      if (dp > 0) {
        ups.push(k);
        pos++;
      }
      else {
        downs.push(k);
        neg++;
      }
      
      if ((i % 100) == 0) console.log({pos, neg})
    }

    const getUp = (i: number) => ups[i];
    const getDown = (i: number) => downs[i];
    run('inv', getUp, getDown);
    */


    /*
    let swapped = 0;

    for (let k = 0; k < samples * retry; ++k) {

      const k1 = getBiasedPoint();
      const k2 = getBiasedPoint();

      const score = swapMaybe(stage, k1, k2, best);
      if (score != null) {
        best = score;
        swapped++;
      }
    }

    const percent = swapped / samples * 100;
    const smooth = smoother(percent);

    if (swapped) {
      logStep(i, best, `(${swapped} / ${samples}) (~${fmt(smooth)}%) (${fmt(percent)}%)`);
    }
    else {
      failed++;
      if ((failed % 10) == 0) {
        logStep(i, '⚠️  No convergence');
      }
    }
    */
    
    /*
    let swapped = 0;

    peaks.length = 0;
    let j = 0;
    for (let i = 0; i < order.length; ++i) {
      const k = order[i];

      if (duds.has(k)) continue;
      peaks[j++] = k;

      if (j === samples) break;
    }

    const n = peaks.length;
    nextPeak: for (let j = 0; j < n; j++) {
      const k1 = peaks[j];
      //const k1 = randomIndex(size);
      
      for (let k = 0; k < retry; k++) {
        const k2 = randomIndex(size);

        const score = swapMaybe(stage, k1, k2, best);
        if (score != null) {
          best = score;
          duds.delete(k2);

          swapped++;
          continue nextPeak;
        }
      }

      duds.add(k1);
    }

    const p = (swapped / peaks.length)*100;
    const s = smoother(p);

    if (swapped) {
      success++;
      failed = 0;

      logStep(i, best, `(${swapped} / ${peaks.length}) (~${(s).toFixed(1)}%) (${(p).toFixed(1)}%) (#${duds.size})`);
    }
    else {
      failed++;
      if ((failed % 10) == 0) logStep(i, '⚠️ No convergence');
    }

    const iter = duds.values();
    for (let j = 0; j < duds.size / 32; ++j) {
      const i = iter.next();
      if (i.done) break;

      duds.delete(i.value);
    }
    */

    if (((i + 1) % checkpoint) == 0) {
      logScore(best);
      logRate(best);
      saveAll(stage, false);
    }

    if (failed > limit) {
      console.warn('🛑 Iteration Aborted!');
      break;
    }
  }
  
  const final = evaluateScore(stage);
  logScore(final);
  logRate(final);
  
};

////////////////////////////////////////////////////////////////////////////////

const saveAll = (state: Stage, verbose: boolean) => {
  if (checkHistogram(stage.vs, stage.size, verbose)) {
    saveSnapshot(stage);
  }
};

logTime('Start-up');

const defaults = {
  size: [64, 64, 16],
  steps: 10,
  checkpoint: 3,
  limit: 1000,

  samples: 128,
  retry: 4,
  focus: 1,
  
  space: 'cosineQ',
  time: 'cosineQ',
  spaceRange: [0,1],
  timeRange: [0,1],
};

const args = yargs(hideBin(process.argv)).argv as any;

const params = loadJSON('params.json');
const resolved = {...defaults, ...params};

const size = args.size ? parseSize(args.size) :resolved.size;
const steps = args.steps != null ? parseNumber(args.steps) : resolved.steps;
const checkpoint = args.checkpoint != null ? parseNumber(args.checkpoint) : resolved.checkpoint;
const limit = args.limit != null ? parseNumber(args.limit) : resolved.limit;

const samples = parseNumber(args.samples) || resolved.samples;
const retry = parseNumber(args.retry) || resolved.retry;
const focus = parseNumber(args.focus) || resolved.focus;

const space = args.space || resolved.space;
const time = args.time || resolved.time;
const spaceRange = args.spaceRange != null ? parsePoint(args.spaceRange).slice(0, 2) : resolved.spaceRange;
const timeRange = args.timeRange != null ? parsePoint(args.timeRange).slice(0, 2) : resolved.timeRange;
const resume = !args.reset;

const options = {size, steps, checkpoint, limit, samples, retry, focus, space, time, spaceRange, timeRange};

console.log('Options', options);
saveJSON(options, 'params.json');

const stage = allocStage(size, space, time, spaceRange, timeRange);

logTime('Alloc');

if (resume && loadState(stage, 'state.png')) {
  copySnapshot('state.png', 'state-last.png');
  logTime('Resume');
}
else {
  initEqual(stage);
  logTime('Init');
}

run(stage, samples, retry, focus, steps, checkpoint, limit);

logTime('Save');

saveAll(stage, true);

logTime('Done');
